package xyz.zanewoodard.hackerrank.algorithms.graphtheory.bfsshortreach;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner is = new Scanner(System.in);
        int q = is.nextInt();

        for(int q_i = 0; q_i < q; q_i++) {

            Short numNodes = is.nextShort();
            int numEdges = is.nextInt();


            HashMap<Short, ArrayList<Short>> adjacencyList = new HashMap<>();

            for(int i = 0; i < numEdges; i++) {
                Short u = is.nextShort();
                Short v = is.nextShort();

                ArrayList<Short> adjacentU = adjacencyList.getOrDefault(u, new ArrayList<>());
                ArrayList<Short> adjacentV = adjacencyList.getOrDefault(v, new ArrayList<>());

                adjacentU.add(v);
                adjacencyList.put(u, adjacentU);

                adjacentV.add(u);
                adjacencyList.put(v, adjacentV);
            }

            Short s = is.nextShort();
            Integer[] distances = new Integer[numNodes];
            distances[s] = 0;

            PriorityQueue<Short> Q = new PriorityQueue<>();
            Q.add(s);
            while(!Q.isEmpty()) {
                Short node = Q.remove();
                for(Short neighbor : adjacencyList.get(node)) {
                    Integer altDist = distances[node] + 1;
                    if(distances[neighbor]==null || distances[neighbor] > altDist) {
                        distances[neighbor] = altDist;
                    }

                }
            }


        }
    }
}