package xyz.zanewoodard.hackerrank.algorithms.warmup.diagonal_distance;

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int N = in.nextInt();

        int primarySum = 0;
        int secondarySum = 0;
        for(int x = 0; x < N; x++) {
            for(int y = 0; y < N; y++) {
                int xy = in.nextInt();
                if(x==y) {
                    primarySum += xy;
                }

                if(x+y==N-1) {
                    secondarySum += xy;
                }
            }
        }

        System.out.println(Math.abs(primarySum - secondarySum));
    }
}
