package xyz.zanewoodard.hackerrank.algorithms.warmup.triplets;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a[] = new int[3];
        for (int i = 0; i < 3; i++) {
            a[i] = in.nextInt();
        }

        int aScore = 0;
        int bScore = 0;
        for (int i = 0; i < 3; i++) {
            int b_i = in.nextInt();
            if(a[i]>b_i) {
                aScore++;
            } else if(b_i > a[i]) {
                bScore++;
            }
        }

        System.out.println(aScore + " " + bScore);

    }
}
