package xyz.zanewoodard.hackerrank.algorithms.warmup.largesum;

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner inputReader = new Scanner(System.in);
        int N = inputReader.nextInt();

        long sum = 0;
        for(int i = 0; i < N; i++) {
            sum += inputReader.nextLong();
        }

        System.out.println(sum);
    }
}
