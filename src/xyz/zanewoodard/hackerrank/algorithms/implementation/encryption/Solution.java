package xyz.zanewoodard.hackerrank.algorithms.implementation.encryption;

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        String message = inputScanner.next();

        int L = message.length();

        int[] dimensions = getDimensions(L);
        int rows = dimensions[0];
        int cols = dimensions[1];

        char[][] encryptionMatrix= new char[rows][cols];

        for(int i = 0; i < L; i++) {
            int row = (int)Math.floor((i / (double) cols));
            int col = i % cols;
            encryptionMatrix[row][col] = message.charAt(i);
        }

        StringBuilder stringBuilder = new StringBuilder();
        for(int col = 0; col < cols; col++) {
            for(int row = 0; row < rows; row++) {
                if(encryptionMatrix[row][col]!=0) {
                    stringBuilder.append(encryptionMatrix[row][col]);
                }
            }
            stringBuilder.append(" ");
        }

        System.out.println(stringBuilder);

    }

    private static int[] getDimensions(int area) {
        int min = (int)Math.floor(Math.sqrt(area));
        int max = (int)Math.ceil(Math.sqrt(area));

        int[] dimensions = new int[]{min, max};
        if(area > min*max) {
            dimensions[0] = max;
        }

        return dimensions;
    }
}