package xyz.zanewoodard.hackerrank.algorithms.implementation;

import java.io.*;
import java.util.*;


public class Solution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x1 = in.nextInt();
        int v1 = in.nextInt();
        int x2 = in.nextInt();
        int v2 = in.nextInt();


        int startingDifference = x1 - x2;
        int speedDifference = v1 - v2;

        String answer = "NO";

        if(speedDifference==0) {
            if(startingDifference==0) {
                answer = "YES";
            }
        } else if (!sameSign(startingDifference, speedDifference)) {
            if (startingDifference % speedDifference == 0) {
                answer = "YES";
            }
        }

        System.out.println(answer);

    }

    private static boolean sameSign(int x, int y) {
        return (x < 0) == (y < 0);
    }
}
