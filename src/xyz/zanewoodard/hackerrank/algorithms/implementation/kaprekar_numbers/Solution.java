package xyz.zanewoodard.hackerrank.algorithms.implementation.kaprekar_numbers;

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        int p = inputScanner.nextInt();
        int q = inputScanner.nextInt();

        for(int x = p; x < q; x++) {
            if(kaprekar(x)) {
                System.out.println(x);
            }
        }
    }

    private static boolean kaprekar(int x) {
        long square = x*x;

        int digits[] = new int[11];

        long tmp = square;
        int i;
        for(i = 0; i < 10 && tmp!=0; i++) {
            digits[digits.length-(i+1)] = (int) tmp % 10;
            tmp /= 10;
        }

        System.out.println(square);
        for(int j = digits.length-i; j < digits.length; j++) {
            int l_sum = 0, r_sum = 0;
            for(int k = digits.length-i; k < digits.length; k++) {
                if(k < j) {
                    l_sum += digits[k];
                } else {
                    r_sum += digits[k];
                }
            }
            System.out.println(String.format("%d %d %d", j, l_sum, r_sum));
        }
        System.out.println();
        System.out.println(Arrays.toString(digits));
        return false;
    }
}