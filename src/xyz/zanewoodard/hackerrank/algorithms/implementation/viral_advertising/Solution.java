package xyz.zanewoodard.hackerrank.algorithms.implementation.viral_advertising;

import java.util.*;

public class Solution {

    private static final long INITIAL_USERS = 5;
    private static final long SHARES_PER_USER = 3;

    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);

        int days = inputScanner.nextInt();

        ArrayList<Long> users = new ArrayList<>(days);
        users.add(0, (long) Math.floor(INITIAL_USERS / 2));

        for(int day = 1; day < days; day++) {
            long newUsers = users.get(day-1) * SHARES_PER_USER;
            long dailyLikes = (long) Math.floor(newUsers / 2);
            users.add(day, (long)dailyLikes);
        }

        System.out.println(users.stream().mapToLong(i -> i).sum());
    }
}