package xyz.zanewoodard.hackerrank.algorithms.dynamic_programming.grid_challenge;

import java.util.*;

public class Solution {

    private static Scanner scanner;
    public static void main(String[] args) {
        scanner = new Scanner(System.in); /*new Scanner("1\n" +
                "5\n" +
                "ebacd\n" +
                "fghij\n" +
                "olmkn\n" +
                "trpqs\n" +
                "xywuv\n");*/

        int T = scanner.nextInt();

        for(int i = 0; i < T; i++) {
            try {
                int N = scanner.nextInt();
                if(canLexiOrder(N)) {
                    System.out.println("YES");
                } else {
                    System.out.println("NO");
                }
            } catch(Exception e) {
                String a = scanner.next();
                System.out.println(a);
            }


        }

        scanner.close();
    }

    private static boolean canLexiOrder(int N) {

        char[] prevLine = scanner.next().toCharArray();
        Arrays.sort(prevLine);
        char[] currLine;
        for(int i = 1; i < N; i++) {
            String line = scanner.next();
            currLine = line.toCharArray();
            Arrays.sort(currLine);

            if(!ordered(prevLine, currLine)) {
                return false;
            }

            prevLine = currLine;
        }

        return true;

    }

    private static boolean ordered(char[] line1, char[] line2) {
        boolean result = true;
        for(int i = 0; i < line1.length; i++) {
            //We COULD use a conditional here and do short circuiting to reduce work
            //But then we're dealing with additional branch prediction which hurts performance
            result &= line2[i] >= line1[i];
        }
        return result;
    }
}