package xyz.zanewoodard.hackerrank.algorithms.dynamic_programming.candies;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Integer N = scanner.nextInt();

        Integer[] ratings = new Integer[N];
        Integer[] candies = new Integer[N];
        Integer min = 0;
        Integer minIndex = 0;

        for(int i = 0; i < N; i++) {
            ratings[i] = scanner.nextInt();
            if(ratings[i] < min) {
                min = ratings[i];
                minIndex = i;
            }
        }


        go(minIndex, ratings, candies);
    }

    private static Integer[] go(Integer index, Integer[] ratings, Integer[] candies) {
        int lIndex = index - 1;
        int rIndex = index - 1;

        if(lIndex > 0 && candies[lIndex]!=0) {
            if(ratings[lIndex] > ratings[index]) {

            } else {

            }

        } else if(rIndex < candies.length && candies[rIndex]!=0) {

        } else {
            candies[index] = 1;
            go(lIndex, ratings, candies);
            go(rIndex, ratings, candies);
        }

        return candies;
    }
}