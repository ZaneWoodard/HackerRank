package xyz.zanewoodard.hackerrank.array_ds;

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner stdinScanner = new Scanner(System.in);
        Integer count = stdinScanner.nextInt();

        //Output is a sort of "String stack", new elements push old ones further to the right
        String output = "";
        for(int x = 0; x < count; x++) {
            output = stdinScanner.next() + " " + output;
        }


        //Above code produces output with a trailing space when count >= 1
        if(count>=1) {
            output = output.substring(0, output.length()-1);
        }

        System.out.print(output);
    }
}