package xyz.zanewoodard.hackerrank.java.advanced.sha256;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.next();
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] sha256Hash = digest.digest(word.getBytes());
            System.out.println(bytesToHexString(sha256Hash));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    private static String bytesToHexString(byte[] hex) {
        StringBuilder sb = new StringBuilder();

        for(byte octet : hex) {
            sb.append(String.format("%02X", octet).toLowerCase());
        }

        return sb.toString();
    }
}