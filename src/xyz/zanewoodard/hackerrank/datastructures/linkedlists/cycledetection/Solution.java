package xyz.zanewoodard.hackerrank.datastructures.linkedlists.cycledetection;

/*
Detect a cycle in a linked list. Note that the head pointer may be 'null' if the list is empty.

A Node is defined as:
    class Node {
        int data;
        Node next;
    }
*/

public class Solution {
    public static void main(String[] args) {

    }

    boolean hasCycle(Node fast) {
        if(fast==null) return false;
        Node slow = fast;

        while(fast.next!=null && fast.next.next!=null) {
            if(fast.next==slow || fast.next.next==slow) {
                return true;
            } else {
                slow = slow.next;
                fast = fast.next.next;
            }
        }
        return false;
    }
}

class Node {
    int data;
    Node next;
}
