package xyz.zanewoodard.hackerrank.array_ds_2d;

import java.util.*;

public class Solution {
    public static void main(String[] args) {

        Scanner stdinScanner = new Scanner(System.in);
        //Build the matrix
        int[][] matrix = new int[6][6];
        for(int x = 0; x < 6; x++) {
            for(int y = 0; y < 6; y++) {
                matrix[x][y] = stdinScanner.nextInt();
            }
        }

        //Find the maximal hourglass, where [x][y] is the topleft corner of the hour glass
        //Due to the size of the hourglass and matrix, all valid hourglasses must have their topleft corner between [0]]0] and [3][3]
        int maxHourglassSum = Integer.MIN_VALUE, hourglassSum;
        for(int x = 0; x < 4; x++) {
            for(int y = 0; y < 4; y++) {
                hourglassSum = sumHourglass(matrix, x, y);
                if(hourglassSum > maxHourglassSum) {
                    maxHourglassSum = hourglassSum;
                }
            }
        }

        System.out.print(maxHourglassSum);
    }

    /**
     * Finds the sum of all the values within the hourglass.
     * @param matrix the matrix to use
     * @param x the row of the top left value
     * @param y the column of the top left value
     * @return the sum of the values in the defined hourglass
     */
    public static int sumHourglass(int[][] matrix, int x, int y) {
        return matrix[x][y] + matrix[x][y+1] + matrix[x][y+2] //Sum the top row
        + matrix[x+1][y+1] //Sum the middle column
        + matrix[x+2][y] + matrix[x+2][y+1] + matrix[x+2][y+2]; //Sum the bottom row

    }
}
