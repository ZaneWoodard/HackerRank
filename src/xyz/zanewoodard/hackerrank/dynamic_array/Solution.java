package xyz.zanewoodard.hackerrank.dynamic_array;

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        int Q = scanner.nextInt();

        List<List<Integer>> seqList = new ArrayList<>(N);
        for(int i = 0; i < N; i++) {
            seqList.add(new ArrayList<>());
        }
        int lastAns = 0;

        for(int i = 0; i < Q; i++) {
            int qType = scanner.nextInt(), x = scanner.nextInt(), y = scanner.nextInt();

            int seqIndex = index(N, lastAns, x);
            List<Integer> seq = seqList.get(seqIndex);

            if(qType==1) {
                seq.add(y);
            } else if(qType==2) {
                int elementIndex = y % seq.size();
                lastAns = seq.get(elementIndex);
                System.out.println(lastAns);
            }

        }
    }

    private static int index(int N, int lastAns, int x) {
        return ((x ^ lastAns) % N);
    }
}
