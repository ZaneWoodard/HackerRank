## Purpose
This repository will be a place for the solutions I write to HackerRank challenges.
The majority should be written in Java, although that may vary depending on the challenge.

Java files will be sorted into packages for organization, although HackerRank solutions should be in the default package
